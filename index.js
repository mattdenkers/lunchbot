import "dotenv/config";
import {
  add,
  giveOptionsForVoting,
  help,
  list,
  recommend,
  remove,
} from "./commands";
import { api } from "@serverless/cloud";

api.post("/", async (req, res) => {
  console.log("got request ->", req.body);

  try {
    // Grab passed-in params
    const params = req.body.text;

    // HELP
    if (params.includes("help")) {
      help(res);
      return;
    }

    // LIST
    if (params.includes("list")) {
      await list(res);
      return;
    }

    // ADD
    if (params.includes("add")) {
      await add(res, params);
      return;
    }

    // REMOVE
    if (params.includes("remove")) {
      await remove(res, params);
      return;
    }

    // RECOMMEND (single option)
    if (params.includes("recommend")) {
      await recommend(req, res);
      return;
    }

    // 3 OPTION VOTING
    await giveOptionsForVoting(req, res);
  } catch (e) {
    console.error(e);
    res.send("Something went wrong... please try again");
  }
});
