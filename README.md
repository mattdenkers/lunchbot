# Lunchbot

- Uses Serverless Cloud
- You'll need a `.env` file with a Slack Token and a Slack Signing Secret

```
SLACK_TOKEN=token
SLACK_SIGNING_SECRET=secret
```
