import { data } from "@serverless/cloud";
import { SLACK_APP, RESTAURANTS, HELP_OPTIONS } from "./constants";
import { getRandom, getMultipleRandom } from "./utils";

export function help(res) {
  // Only visible to requesting user
  res.send(HELP_OPTIONS.join("\n"));
}

export async function list(res) {
  const restaurants = await data.get(RESTAURANTS);

  // Only visible to requesting user
  res.send(restaurants.join("\n"));
}

export async function add(res, params) {
  // Get the restaurant name
  const name = params.split("add ")[1];

  // Get existing restaurants
  const restaurants = await data.get(RESTAURANTS);

  // Add new one
  restaurants.push(name);

  // Update store (without waiting)
  data.set(RESTAURANTS, restaurants);

  // Only visible to requesting user
  res.send(`Added ${name} to the list of restaurant possibilities!`);
}

export async function remove(res, params) {
  // Get the restaurant name
  const name = params.split("remove ")[1];

  // Get existing restaurants
  const restaurants = await data.get(RESTAURANTS);

  // Find restaurant to remove
  const index = restaurants.indexOf(name);

  if (index >= 0) {
    // Remove and set
    restaurants.splice(index, 1);

    // Don't wait
    data.set(RESTAURANTS, restaurants);

    // Only visible to requesting user
    res.send(`Removed ${name} from the list of restaurant possibilities!`);
    return;
  }

  res.send(
    `Failed to remove ${name} - be sure to copy/paste the name from the list for an exact match`
  );
}

export async function recommend(req, res) {
  const restaurants = await data.get(RESTAURANTS);

  // Intentionally not waiting, visible to everyone in channel
  SLACK_APP.client.chat.postMessage({
    token: process.env.SLACK_TOKEN,
    channel: req.body.channel_id,
    text: `Today is a ${getRandom(restaurants)} day 🤤`,
  });

  res.send();
}

export async function giveOptionsForVoting(req, res) {
  const restaurants = await data.get(RESTAURANTS);

  const lines = ["Today's options are:", ""]
    .concat(getMultipleRandom(restaurants, 3))
    .concat(["", "Use emojis to vote!"]);

  // Intentionally not waiting, visible to everyone in channel
  SLACK_APP.client.chat.postMessage({
    token: process.env.SLACK_TOKEN,
    channel: req.body.channel_id,
    text: lines.join("\n"),
  });

  res.send();
}
