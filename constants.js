// This import looks weird because of something strange in the Serverless Cloud env
import pkg from "@slack/bolt";
const { App } = pkg;

// Instantiate Slack app
export const SLACK_APP = new App({
  signingSecret: process.env.SLACK_SIGNING_SECRET,
  token: process.env.SLACK_TOKEN,
});

// Key for data store
export const RESTAURANTS = "restaurants";

// Help options
export const HELP_OPTIONS = [
  "`/lunchbot help` to see all available commands",
  "`/lunchbot list` to see all restaurant possibilities",
  "`/lunchbot add <name>` to add a restaurant possibility. Ex: `/lunchbot add Pizza Planet 🍕` or `/lunchbot add Wallaby's 🦘`",
  "`/lunchbot remove <name>` to remove a restaurant possibility. Ex: `/lunchbot remove Pizza Planet 🍕` or `/lunchbot remove Wallaby's 🦘`",
  "`/lunchbot recommend` to get a lunch recommendation",
  "`/lunchbot` to get 3 options for people to vote on with emojis",
];
